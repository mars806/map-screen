package com.mars.mapscreen.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.mars.mapscreen.databinding.FragmentAirQualityMapBinding;

public class AirQualityMapFragment extends Fragment {

    FragmentAirQualityMapBinding mapBinding;
    private AirQualityMapViewModel airQualityMapViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        airQualityMapViewModel =
                ViewModelProviders.of(this).get(AirQualityMapViewModel.class);
        mapBinding = FragmentAirQualityMapBinding.inflate(inflater, container, false);
        return mapBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WebView webView = mapBinding.webView;
        webView.setBackgroundColor(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.clearCache(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl("https://aqicn.org/nearest");

    }
}