package com.mars.mapscreen.ui.home;


import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.mars.mapscreen.model.CityModel;
import com.mars.mapscreen.model.forcast_data.Daily;
import com.mars.mapscreen.model.forcast_data.ForecastModel;
import com.mars.mapscreen.repository.HistoryRepository;
import com.mars.mapscreen.repository.WeatherRepository;
import com.mars.mapscreen.response.WeatherResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class HomeViewModel extends AndroidViewModel {

    private String TAG = HomeViewModel.class.getSimpleName();

    private MediatorLiveData<WeatherResponse> mWeatherResponse;
    private WeatherRepository weatherRepository;
    private HistoryRepository historyRepository;
    MutableLiveData<String> currentCity = new MutableLiveData<>();

    private CompositeDisposable disposable;
    private MutableLiveData<List<Daily>> forecastListLiveData = new MutableLiveData<>();
    private MutableLiveData<Throwable> forecastErrorLiveData = new MutableLiveData<>();

    /**
     * constructor of viewModel
     * get instance of repositories and
     *      a CompositeDisposable
     * @param application context to get shared preferences
     */
    public HomeViewModel(Application application) {
        super(application);
        mWeatherResponse = new MediatorLiveData<>();
        weatherRepository = WeatherRepository.getInstance();
        historyRepository = HistoryRepository.getInstance(application);
        disposable = new CompositeDisposable();
    }

    /**
     * a weather data holder that observe within a given lifecycle.
     *
     * @param city name of the city
     * @return response of request as LiveData<WeatherResponse>
     */
    public LiveData<WeatherResponse> getWeatherData(String city) {
        mWeatherResponse.addSource(weatherRepository.getWeatherData(city), weatherResponse ->
                mWeatherResponse.setValue(weatherResponse));
        return mWeatherResponse;
    }

    /**
     * get all cities are in the DB
     * @return list of CityModels
     */
    public List<CityModel> getPastCities(){
        return historyRepository.getPastCities();
    }

    /**
     * save city name in DB
     * post current city name to observers
     * @param cityName name of the city
     */
    public void saveCityToDatabase(String cityName){
        currentCity.postValue(cityName);
        historyRepository.saveCityToDatabase(cityName);
    }

    /**
     * current city name holder that can be observed within a given lifecycle.
     * @return current city name
     */
    public MutableLiveData<String> getCurrentCity(){
        return currentCity;
    }

    /**
     * list of Dailies that can be observed within a given lifecycle.
     * this list will be updated by calling {@link  com.mars.mapscreen.repository.WeatherRepository#getForecastData(Map)}
     * @return current city name
     */
    public LiveData<List<Daily>> getForecast(){
        return forecastListLiveData;
    }

    /**
     * current city name holder that can be observed within a given lifecycle.
     * Throwable execute in {@link  #getForecastListLiveData(Map)}
     * @return Throwable LiveData
     */
    public LiveData<Throwable> getForecastError(){
        return forecastErrorLiveData;
    }

    /**
     * we have two MutableLiveData object (forecastErrorLiveData and forecastListLiveData)
     * if response is successful post data to forecastListLiveData
     *      else post forecastErrorLiveData
     * @param params map that contains lat and long of city coordinate
     * @return list of dailies LiveData
     */
    public LiveData<List<Daily>> getForecastListLiveData(Map<String, String> params) {
        disposable.add(weatherRepository.getForecastData(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<ForecastModel>>() {
                    @Override
                    public void onNext(Response<ForecastModel> forecastModelResponse) {
                        if (forecastModelResponse.isSuccessful()) {
                            ForecastModel forecastModel = forecastModelResponse.body();
                            if (forecastModel != null) {
                                List<Daily> dailyList = forecastModel.getDaily();
                                forecastListLiveData.postValue(dailyList);
                                Log.i(TAG, "onComplete: " + dailyList.size());

                            } else
                                forecastErrorLiveData.postValue(null);
                        } else
                            forecastErrorLiveData.postValue(null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        forecastErrorLiveData.postValue(e);
                        Log.i(TAG, "onError: "+ e);
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete: forecast");

                    }
                }));

        return forecastListLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}