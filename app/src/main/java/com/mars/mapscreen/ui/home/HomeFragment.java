package com.mars.mapscreen.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.mars.mapscreen.R;
import com.mars.mapscreen.adapter.CityAdapter;
import com.mars.mapscreen.adapter.MainAdapter;
import com.mars.mapscreen.databinding.FragmentHomeBinding;
import com.mars.mapscreen.model.CityModel;
import com.mars.mapscreen.model.MessageEvent;
import com.mars.mapscreen.model.TempModel;
import com.mars.mapscreen.model.forcast_data.Daily;
import com.mars.mapscreen.model.forcast_data.Temp;
import com.mars.mapscreen.model.weather_data.Coord;
import com.mars.mapscreen.model.weather_data.DataModel;
import com.mars.mapscreen.model.weather_data.Main;
import com.mars.mapscreen.utils.Preferences;
import com.mars.mapscreen.utils.SetupCharts;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeFragment extends Fragment {

    String TAG = HomeFragment.class.getSimpleName();

    private HomeViewModel homeViewModel;
    Activity activity;

    FragmentHomeBinding binding;
    SearchListener listener;

    List<CityModel> cityModels;
    CityAdapter cityAdapter;

    List<TempModel> tempModels = new ArrayList<>();
    MainAdapter mainAdapter;
    MutableLiveData<DataModel> dataModelLiveData = new MutableLiveData<>();

    Preferences preferences;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (SearchListener) context;
        activity = (Activity) context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView();
        initMainRecyclerView();
        configureProgressView();
        performLocation();


        homeViewModel.getCurrentCity().observe(getViewLifecycleOwner(), this::updateCityAdapter);
        dataModelLiveData.observe(getViewLifecycleOwner(), this::getForecastData);
        homeViewModel.getForecast().observe(getViewLifecycleOwner(), this::setupChart);
        homeViewModel.getForecastError().observe(getViewLifecycleOwner(), this::showForecastError);

        /*
        when recyclerView scrolled, center item is changing so
                 get new data for current city with adapter position
                 and change current city name in preferences
         */
        binding.recyclerView.addOnItemChangedListener((viewHolder, adapterPosition) -> {
            CityModel cityModel = cityModels.get(adapterPosition);
            String cityName = cityModel.getCityName();
            getWeatherData(new MessageEvent(cityName));
            saveCurrentCityInPreferences(cityName);
        });
    }

    /**
     *  show error of get forecast data
     * @param throwable error of get forecast data
     */
    private void showForecastError(Throwable throwable) {
        if (throwable != null)
            binding.historyChart.setNoDataText(throwable.getMessage());
        else
            binding.historyChart.setNoDataText("GET DATA ERROR!");

        binding.historyChart.invalidate();
    }

    /**
     * setup chart with entry points and labels of x axis
     * animate chart (no need to call chart.invalidate)
     * @param dailyList list that contains forecast data
     */
    private void setupChart(List<Daily> dailyList) {
        if (dailyList != null) {
            ArrayList<Entry> entries = getEntries(dailyList);
            String[] labels = getChartLabels(dailyList);

            LineData data = new SetupCharts().drawChart(activity, entries, binding.historyChart, labels);
            binding.historyChart.setData(data);
            binding.historyChart.animateY(55 * 35, Easing.Linear);
        }
    }

    /**
     *
     * @param dailyList list that contains forecast data
     * @return arrayList of entry(chart points)
     */
    private ArrayList<Entry> getEntries(List<Daily> dailyList) {
        ArrayList<Entry> entries = new ArrayList<>();
        int i = 0;
        while ( i < dailyList.size()){
            Daily daily = dailyList.get(i);
            Temp temp = daily.getTemp();
            double y = getCelsius(temp.getEve());
            entries.add(new Entry(i, convertDoubleToInteger(y)));
            i++;
        }
        return entries;
    }

    /**
     * get array of x axis labels that is date values
     *      formatted date type is "dd.MM'
     * @param dailyList list that contains forecast data
     * @return array of x axis labels
     */
    private String[] getChartLabels(List<Daily> dailyList) {

        String[] labels = new String[dailyList.size()];

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());

        for (int i = 0; i < dailyList.size(); i++){
            Daily daily = dailyList.get(i);
            Date date = new Date(daily.getDt() * 1000);
            labels[i] = dateFormat.format(date.getTime());
        }

        return labels;
    }

    /**
     * convert Double To Integer
     * @param d double value
     * @return integer value
     */
    private int convertDoubleToInteger(double d){
        return (int)Math.round(d);
    }

    /**
     * get coordinate of city and
     *      set parameter and request call
     * @param dataModel model that contains coordinate of city
     */
    private void getForecastData(DataModel dataModel) {
        Map<String, String> params = new HashMap<>();
        Coord coord = dataModel.getCoord();
        Double lat = coord.getLat();
        Double lon = coord.getLon();
        params.put("lat",lat.toString());
        params.put("lon", lon.toString());
        homeViewModel.getForecastListLiveData(params);
    }

    /**
     * when new a new city is searched, add it in recyclerview
     *      and scroll to position of it
     * @param cityName name of city
     */
    private void updateCityAdapter(String cityName){
        int index = findCurrentCityPosition(cityName);
        if (index == -1) {// city isn't in cityModels list( we have a new city)
            CityModel cityModel = new CityModel(cityName);
            cityModels.add(cityModel);
            cityAdapter.addNewCity(cityModels);

            int position = findCurrentCityPosition(cityName);
            scrollRecyclerviewToCurrentCityPosition(position);
        }
    }

    /*
     when dataModelData changed, change recyclerView configurations
         reset recyclerView data when by each city changes
         scroll to middle of items(always there is just 3 items)
     */
    private void initMainRecyclerView() {
        dataModelLiveData.observe(getViewLifecycleOwner(), dataModel -> {
            Main main = dataModel.getMain();
            mainAdapter = new MainAdapter();
            binding.mainRecyclerView.setAdapter(mainAdapter);
            mainAdapter.resetAdapter(getTempList(main));
            binding.mainRecyclerView.scrollToPosition(1);
        });
    }

    //create temperature list
    private List<TempModel> getTempList(Main main) {
        tempModels.clear();

        tempModels.add(new TempModel("Max Temp", getCelsius(main.getTempMax()),
                ResourcesCompat.getDrawable(activity.getResources(),R.drawable.ic_hot, null)));
        tempModels.add(new TempModel("Current Temp", getCelsius(main.getTemp()),
                ResourcesCompat.getDrawable(activity.getResources(),R.drawable.ic_celsius, null)));
        tempModels.add(new TempModel("Min Temp",getCelsius(main.getTempMin()),
                ResourcesCompat.getDrawable(activity.getResources(),R.drawable.ic_cold, null)));

        return tempModels;
    }

    /**
     * convert kelvin to celsius
     * @param kelvin temperature in kelvin
     * @return temperature in celsius
     */
    public double getCelsius(double kelvin){
        return kelvin - 273.15;
    }

    /*
    get list of cities
    get instance of adapter
    set adapter recyclerView
     */
    private void initRecyclerView() {
        cityModels = homeViewModel.getPastCities();
        cityAdapter = new CityAdapter(cityModels);
        binding.recyclerView.setAdapter(cityAdapter);
        cityAdapter.notifyDataSetChanged();
    }


    /**
     * save city name in preferences
     * @param cityName name of the city
     */
    private void saveCurrentCityInPreferences(String cityName) {
        preferences = Preferences.getInstance(activity);
        preferences.setLocation(cityName);
    }

    /*
    set max value of progressbar
     */
    private void configureProgressView() {
        binding.circularProgress.setMaxProgress(100);
    }

    /**
     * check if there is location in preferences
     *      search whether, else show location dialog
     */
    private void performLocation() {
        String location = getLocation();
        if (location != null){
            getWeatherData(new MessageEvent(location));
        }else {
            listener.showLocationDialog();
        }
    }

    /**
     * get location(city name) that is in preferences
     * @return location(city name)
     */
    private String getLocation() {
        Preferences preferences = new Preferences(activity.getApplicationContext());
        return preferences.getLocation();
    }

    /**
     * this method update by each city changes.
     * scroll RecyclerView to city position
     * get city whether data from viewModel
     * @param event model that contains city name
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getWeatherData(MessageEvent event) {
        int position = findCurrentCityPosition(event.getText());
        scrollRecyclerviewToCurrentCityPosition(position);
        /*
        get city whether data from viewModel
         */
        homeViewModel.getWeatherData(event.getText()).observe(getViewLifecycleOwner(), weatherResponse -> {
            /*
            if whetherResponse is null, maybe we don't have that city
                or serialization of model class is wrong
             */
            if (weatherResponse == null) {
                // handle null error
                Toast.makeText(activity, "Get data error", Toast.LENGTH_LONG).show();
                Log.e(TAG, "Data response is null");
                return;
            }

            /*
            if we weatherResponse is'nt null and
                getError is null, so we have successful call

            if call is successful so
                save city name in preferences
                set dataModel into dataModelLiveData
                get humidity and set circular progressbar and its textView

             */
            if (weatherResponse.getError() == null) {
                // call is successful
                saveCity(event.getText());
                Log.i(TAG, "Data response is " + weatherResponse.getDataModel());
                DataModel dataModel = weatherResponse.getDataModel();
                dataModelLiveData.setValue(dataModel);
                Main main = dataModel.getMain();
                int humidity = main.getHumidity();
                binding.circularProgress.setCurrentProgress(humidity);
                binding.txtProgress.setText(humidity + "%");

            } else {
                // call failed.
                Throwable e = weatherResponse.getError();
                Toast.makeText(activity, "Error is " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error is " + e.getLocalizedMessage());
            }
        });

    }

    /**
     * get cityPosition and if exist scroll recyclerView
     * @param position position of current city
     */
    private void scrollRecyclerviewToCurrentCityPosition(int position) {
        if (position == -1)
            DataException("City Not Found");
        else
            binding.recyclerView.scrollToPosition(position);
    }

    /**
     *  if cityName is in cityModels list, return position
     *      else return -1
     *
     * @param cityName name of the city
     * @return cityName position in the cityModels list
     */
    private int findCurrentCityPosition(String cityName){
        int index = -1; // if city exist in database return position else return -1
        for (int position = 0; position < cityModels.size(); position++) {
            CityModel cityModel = cityModels.get(position);
            if (cityModel.getCityName().equals(cityName)) {
                index = position;
                return index;
            }
        }
        return index;
    }

    /**
     * show exception message
     * @param message exeption message
     */
    private void DataException(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    /**
     * save city name to database\
     * @param cityName name of the city
     */
    private void saveCity(String cityName) {
        homeViewModel.saveCityToDatabase(cityName);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}