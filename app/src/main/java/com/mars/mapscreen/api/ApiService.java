package com.mars.mapscreen.api;

import com.mars.mapscreen.model.forcast_data.ForecastModel;
import com.mars.mapscreen.model.weather_data.DataModel;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiService {

    /**
     *
     * @param city name of the city
     * @return call response
     */
    @GET("data/2.5/weather")
    Call<DataModel> getWeatherData(@Query("q")String city);

    /**
     *
     * @param params hashMap that contains lat, lon coordinate
     * @return request response
     */
    @GET("data/2.5/onecall")
    Observable<Response<ForecastModel>> getForecastModel(@QueryMap Map<String, String> params);
}
