package com.mars.mapscreen.api;


import com.mars.mapscreen.utils.Constants;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient{

    //logging interceptor that log request calls
    private static HttpLoggingInterceptor getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        return logging.setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    //set auth key to retrofit request as query parameter
    private static Interceptor authorizationInterceptor = chain -> {
        okhttp3.Request request = chain.request();

        //create url with token as query parameter
        HttpUrl url = request
                .url()
                .newBuilder()
                .addQueryParameter("appid", Constants.AUTH_TOKEN)
                .build();

        //create okhttp request with custom httpUrl
        request = request
                .newBuilder()
                .url(url)
                .build();

        return chain.proceed(request);
    };

    /*
    create OkHttpClient with interceptors
     */
    private static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .addInterceptor(getClient())
            .build();


    private static Retrofit retrofit;
    private static final String BASE_URL = "https://api.openweathermap.org/";

    /**
     * create retrofit object and add
     *          RxJava2CallAdapterFactory and GsonConverterFactory
     * @return retrofit object
     */
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}