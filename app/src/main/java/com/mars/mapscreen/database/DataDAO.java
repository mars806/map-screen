package com.mars.mapscreen.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mars.mapscreen.model.CityModel;

import java.util.List;

@Dao
public interface DataDAO {

    @Query("SELECT * FROM cities")
    List<CityModel> get_all_cities();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCityInDatabase(CityModel cityModel);


}
