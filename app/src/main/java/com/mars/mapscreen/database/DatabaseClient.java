package com.mars.mapscreen.database;

import android.content.Context;

import androidx.room.Room;

public class DatabaseClient {

    private static DatabaseClient mInstance;

    //our app database object
    private AppDatabases appDatabase;

    private DatabaseClient(Context context) {

        //creating the app database with Room database builder
        appDatabase = Room.databaseBuilder(context, AppDatabases.class, "info.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDatabases getAppDatabase() {
        return appDatabase;
    }

}
