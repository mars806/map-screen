package com.mars.mapscreen.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.mars.mapscreen.model.CityModel;


@Database(entities = {CityModel.class}, version = 3)
public abstract class AppDatabases extends RoomDatabase {

    public abstract DataDAO dataDAO();

}
