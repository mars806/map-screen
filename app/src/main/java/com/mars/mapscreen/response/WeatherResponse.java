package com.mars.mapscreen.response;

import com.mars.mapscreen.model.weather_data.DataModel;

public class WeatherResponse {

    public DataModel dataModel;
    private Throwable error;

    public WeatherResponse(DataModel dataModel) {
        this.dataModel = dataModel;
        this.error = null;
    }

    public WeatherResponse(Throwable error) {
        this.error = error;
        this.dataModel = null;
    }

    public DataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }
}
