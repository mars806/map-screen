package com.mars.mapscreen.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static Preferences mInstance;
    private SharedPreferences pref;
    int PRIVATE_MODE = 0;

    public Preferences(Context context) {
        //get getSharedPreferences
        pref = context.getSharedPreferences(Constants.LOCATION_KEY, PRIVATE_MODE);
    }

    /**
     * create a singleton instance of preferences
     * @param context context of application
     * @return instance of class
     */
    public static synchronized Preferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Preferences(context);
        }
        return mInstance;
    }


    /**
     * save cityName in shared preferences
     * @param cityName name of the city
     */
    public void setLocation(String cityName) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.LOCATION_KEY, cityName);
        editor.apply();
    }

    /**
     * get name of the city
     * @return name of the city
     */
    public String getLocation(){
        return pref.getString(Constants.LOCATION_KEY, null);
    }
}
