package com.mars.mapscreen.utils;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.mars.mapscreen.R;

import java.util.ArrayList;

public class SetupCharts {

    public LineData drawChart(Context context, ArrayList<Entry> entries, LineChart chart, String[] labels) {

        try {

            if (entries.size() > 0) {
                //customization dataSet Settings
                LineDataSet set = new LineDataSet(entries, "Forecast");
                set.setAxisDependency(YAxis.AxisDependency.LEFT);
                set.setDrawValues(false);
                set.setLineWidth(3f);
                set.setDrawCircles(false);
                set.setValueTextSize(12f);
                set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//                set.setValueTextColor(context.getResources().getColor(R.color.progress));
//                set.setColor(context.getResources().getColor(R.color.progress));
//                set.setCircleColor(context.getResources().getColor(R.color.progress));

                // gradient of line
                Paint paint = chart.getRenderer().getPaintRender();
                int height = chart.getHeight();
                LinearGradient linGrad = new LinearGradient(0, 0, 0, height,
                        context.getResources().getColor(android.R.color.holo_red_light),
                        context.getResources().getColor(android.R.color.holo_blue_light),
                        Shader.TileMode.REPEAT);
                paint.setShader(linGrad);

                //customization chart settings
                chart.setNoDataText("No data");
                chart.setTouchEnabled(true);
                chart.setScaleEnabled(false);
                chart.setDragEnabled(false);
                chart.setDrawGridBackground(false);
                chart.getDescription().setEnabled(false);
                chart.setPinchZoom(false);
                chart.setNestedScrollingEnabled(false);
                chart.setExtraBottomOffset(20);
                chart.setExtraLeftOffset(16);
                chart.setExtraRightOffset(8);

                chart.getLegend().setEnabled(false);

                if (labels != null) {
                    //customization XAxis settings
                    XAxis x1 = chart.getXAxis();
                    x1.setTextColor(context.getResources().getColor(R.color.martini));
                    x1.setDrawGridLines(false);
                    x1.setAvoidFirstLastClipping(true);
                    x1.setPosition(XAxis.XAxisPosition.BOTTOM);
                    x1.setValueFormatter(new IndexAxisValueFormatter(labels));
                    x1.setLabelCount(labels.length, true);

                }

                //customization YAxis settings
                YAxis y1 = chart.getAxisLeft();
                y1.setDrawZeroLine(true);
                y1.setDrawLabels(true);
                y1.setTextColor(context.getResources().getColor(R.color.martini));
                y1.setDrawGridLines(true);
                y1.setDrawAxisLine(false);


                YAxis y2 = chart.getAxisRight();
                y2.setEnabled(false);

                //set custom font
//                Typeface face = Typeface.createFromAsset(context.getAssets(), "Shabnamn.ttf");
//                data.setValueTypeface(face);
//                chart.getXAxis().setTypeface(face);

                return new LineData(set);

            } else
                return null;

        }catch (Exception e){
            Log.e("setupCharts", "drawChart: " + e );
            e.printStackTrace();
            return null;
        }
    }
}
