package com.mars.mapscreen.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Cities")
public class CityModel {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "city_name")
    String cityName;

    public CityModel(@NonNull String cityName) {
        this.cityName = cityName;
    }

    @NonNull
    public String getCityName() {
        return cityName;
    }
}
