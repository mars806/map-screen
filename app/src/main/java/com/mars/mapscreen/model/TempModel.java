package com.mars.mapscreen.model;

import android.graphics.drawable.Drawable;

public class TempModel {

    String type;
    Double temperature;
    Drawable icon;

    public TempModel(String type, Double temperature, Drawable icon) {
        this.type = type;
        this.temperature = temperature;
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Drawable getIcon() {
        return icon;
    }
}

