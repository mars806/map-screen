package com.mars.mapscreen.model;

public class MessageEvent {
    private String text;

    public MessageEvent(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
