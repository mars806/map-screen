package com.mars.mapscreen.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mars.mapscreen.api.ApiClient;
import com.mars.mapscreen.api.ApiService;
import com.mars.mapscreen.model.forcast_data.ForecastModel;
import com.mars.mapscreen.model.weather_data.DataModel;
import com.mars.mapscreen.response.WeatherResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WeatherRepository {

    private static WeatherRepository weatherRepository;
    ApiService apiService;

    /**
     * get instance of this class
     * @return instance of WeatherRepository
     */
    public static WeatherRepository getInstance() {
        if (weatherRepository == null) {
            weatherRepository = new WeatherRepository();
        }
        return weatherRepository;
    }

    /**
     * constructor of this class
     * inti apiService
     */
    public WeatherRepository() {
        apiService = ApiClient.getRetrofitInstance().create(ApiService.class);
    }

    /**
     * get weather data from server
     * @param city query parameter
     * @return  weatherResponse
     */
    public LiveData<WeatherResponse> getWeatherData(String city) {

        final MutableLiveData<WeatherResponse> weatherResponse = new MutableLiveData<>();
        Call<DataModel> currentWeatherCall = apiService.getWeatherData(city);
        currentWeatherCall.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(@NonNull Call<DataModel> call, @NonNull Response<DataModel> response) {
                if (response.isSuccessful()){
                    weatherResponse.postValue(new WeatherResponse(response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DataModel> call, @NonNull Throwable t) {
                weatherResponse.postValue(new WeatherResponse(t));
            }
        });

        return weatherResponse;
    }

    /**
     * get forecast data from server
     * @param param hashMap that contains coordinate
     * @return request response
     */
    public Observable<Response<ForecastModel>> getForecastData(Map<String, String> param){
        return apiService.getForecastModel(param);
    }
}