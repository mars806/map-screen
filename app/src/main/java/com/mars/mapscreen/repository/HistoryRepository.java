package com.mars.mapscreen.repository;

import android.app.Application;

import com.mars.mapscreen.database.AppDatabases;
import com.mars.mapscreen.database.DatabaseClient;
import com.mars.mapscreen.model.CityModel;

import java.util.List;

public class HistoryRepository {
    private static HistoryRepository historyRepository;
    AppDatabases databases;

    /**
     * get instance of this class
     * @param application context of application
     * @return instance of HistoryRepository
     */
    public static HistoryRepository getInstance(Application application) {
        if (historyRepository == null) {
            historyRepository = new HistoryRepository(application);
        }
        return historyRepository;
    }

    /**
     *
     * @param application context of database
     */
    public HistoryRepository(Application application) {
        databases = DatabaseClient.getInstance(application).getAppDatabase();
    }

    /**
     * get all cities are in the DB
     * @return list of CityModels
     */
    public List<CityModel> getPastCities(){
        return databases.dataDAO().get_all_cities();
    }

    /**
     * save city name in DB
     * @param cityName name of the city
     */
    public void saveCityToDatabase(String cityName){
        CityModel cityModel = new CityModel(cityName);
        databases.dataDAO().insertCityInDatabase(cityModel);
    }
}