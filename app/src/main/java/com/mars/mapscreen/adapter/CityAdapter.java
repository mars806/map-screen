package com.mars.mapscreen.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mars.mapscreen.R;
import com.mars.mapscreen.model.CityModel;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.MyViewHolder> {

    int maxWidth = 0;

    private List<CityModel> cityModels;

    /**
     * constructor of this class
     * @param cityModels list of cities
     */
    public CityAdapter(List<CityModel> cityModels){
        this.cityModels = cityModels;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_city_item, parent, false);
        return new MyViewHolder(itemView);
    }

    /**
     * set new list and notify adapter
     * @param cityModels new cityModels list
     */
    public void addNewCity(List<CityModel> cityModels){
        this.cityModels = cityModels;
        notifyItemInserted(cityModels.size());
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        /*
        because width of items is not the same,
                keep max width of items and remeasure layout if max width changed
         */
        re_measurementLayoutWidth(holder);
        CityModel model = cityModels.get(position);
        holder.bind(model);
    }

    /*
    if width of this item was bigger than others
       re_measure layout width and change maxWidth value
     */
    private void re_measurementLayoutWidth(MyViewHolder holder){
        int width = holder.itemView.getWidth();
        if (width > maxWidth) {
            holder.itemView.requestLayout();
            maxWidth = width;
        }
    }

    @Override
    public int getItemCount() {
        return cityModels.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txtCity;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCity = itemView.findViewById(R.id.txtCity);

        }

        void bind(CityModel model){

            txtCity.setText(model.getCityName());

        }
    }
}
