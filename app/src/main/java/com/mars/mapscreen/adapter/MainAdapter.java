package com.mars.mapscreen.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mars.mapscreen.R;
import com.mars.mapscreen.model.TempModel;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {

    private List<TempModel> tempModels = new ArrayList<>();

    public MainAdapter(){

    }

    // reset adapter works when another city be selected
    // this method display new data in recyclerView
    public void resetAdapter(List<TempModel> tempModels){
        this.tempModels.clear();
        this.tempModels.addAll(tempModels);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_main_items, parent, false);

        /*
        set item width equal 55% of screen
         */
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() * 0.55);
        itemView.setLayoutParams(layoutParams);

        return new MainAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.MyViewHolder holder, int position) {
        TempModel model = tempModels.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return tempModels.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtType;
        ImageView imgIcon;
        TextView txtTemp;
        View container;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtType = itemView.findViewById(R.id.txtType);
            txtTemp = itemView.findViewById(R.id.txtTemp);
            imgIcon = itemView.findViewById(R.id.imgIcon);
            container = itemView;
        }

        @SuppressLint("SetTextI18n")
        void bind(TempModel model){
            txtType.setText(model.getType());
            imgIcon.setImageDrawable(model.getIcon());
            //cast double to int value
            txtTemp.setText((int)Math.round(model.getTemperature()) + " C°");
        }
    }
}
