package com.mars.mapscreen;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.mars.mapscreen.databinding.ActivityMainBinding;
import com.mars.mapscreen.model.MessageEvent;
import com.mars.mapscreen.ui.home.SearchListener;
import com.mars.mapscreen.utils.Preferences;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

/*
 * Created by Hossein on 8/11/2020
 *
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, SearchListener {

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;
    Toolbar toolbar;
    TextView txtTitle, txtLocation;
    AlertDialog alertDialog;
    AlertDialog.Builder dialogBuilder;

    ActivityMainBinding mainBinding;
    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        bind view
         */
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mainBinding.getRoot());

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);// disable default title

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        changeDrawerIcon();

        txtTitle = findViewById(R.id.txtTitle);
        txtLocation = findViewById(R.id.txtLocation);
        txtTitle.setOnClickListener(this);
        txtLocation.setOnClickListener(this);

        //get instance of preferences
        preferences = Preferences.getInstance(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
    This method is called whenever the user chooses to navigate Up
        within fragments.
     */
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtTitle:
            case R.id.txtLocation:
                showChooseLocationDialog();
                break;
        }
    }

    /**
     * this method called when there is not any
     *      selected location in preferences
     */
    @Override
    public void showLocationDialog() {
        showChooseLocationDialog();
    }

    /**
     * this method create dialog and handel its functionality
     *
     */
    private void showChooseLocationDialog() {
        dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View layoutView = getLayoutInflater().inflate(R.layout.dialog_search_location, null);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        /*
        settings of dialog
        set background color transparent
        and margin from window 20 dp
         */
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 20); //set margin of dialog
        Window window = alertDialog.getWindow();
        if (window != null) {
            alertDialog.getWindow().setBackgroundDrawable(inset);
            // set custom animation
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }
        alertDialog.setCancelable(false);
        alertDialog.show();


        /*
        hide keyboard when user clicked on
            action button on keyboard
         */
        EditText editText = layoutView.findViewById(R.id.searchView);
        editText.setOnEditorActionListener((v, actionId, event) -> {
            hideKeyboard(v);
            return false;
        });

        /*
        get editText value(city name) if not empty
            and send it to homeFragment to get data from server
         */
        Button searchButton = layoutView.findViewById(R.id.btnDialog);
        searchButton.setOnClickListener(view -> {
            String searchValue = editText.getText().toString();
            if (!searchValue.equals("")) {
                //Send editText value to homeFragment
                EventBus.getDefault().post(new MessageEvent(searchValue));
                saveCurrentCityInPreferences(searchValue);
                alertDialog.dismiss();
            }else
                editText.setError("Enter your city name");
        });
    }

    /**
     * save value(location) in preferences
     * @param searchValue the value(location) that inserted in editText in ChooseLocationDialog
     */
    private void saveCurrentCityInPreferences(String searchValue) {
        preferences.setLocation(searchValue);
    }

    /**
     *Request to hide the soft input window from the context of the window that is currently accepting input.
     * @param v view: context of the window that is currently accepting input.
     */
    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /**
     *customization drawable of navigation drawer
     */
    private void changeDrawerIcon() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawer,
                toolbar, R.string.drawer_open, R.string.drawer_close);
        //disable  drawerIndicator(default is true)
        drawerToggle.setDrawerIndicatorEnabled(false);
        // set my drawable for drawer icon
        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_baseline_dehaze_24);
        // onClick event of drawerToggle to open or close drawer
        drawerToggle.setToolbarNavigationClickListener(view -> drawer.openDrawer(GravityCompat.START));
    }

}