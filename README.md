## Weather App


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)

## General info
This is a useful android app that shows weather informations like
 maximum and minimum temperature, humidity and air quality based on your City.
	
## Technologies
Project is created with:
* Navigation components
* Room database
* RxJava version: 2.1.0
* Retrofit library version: 2.8.1
* MPAndroidChart library